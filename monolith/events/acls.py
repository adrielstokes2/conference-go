from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    params = {
        "query": f"{city} {state}"
    }
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    return {"picture_url": content["photos"][0]["src"]["original"]}



def get_weather_data(city, state):
    #getting long and lat
    url = "http://api.openweathermap.org/geo/1.0/direct?"

    params = {
        "q": f"{city}, {state}, USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    long = content[0]["lon"]

    #getting weather


    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": long,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    response = requests.get(url, params=params)
    content = json.loads(response.content)
    temp = content["main"]["temp"]
    weather_description = content["weather"][0]["description"]

    return {"temp": temp, "weather": weather_description}
